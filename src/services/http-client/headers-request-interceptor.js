export const headersRequestInterceptor = (config) => {
  if (!config.headers) config.headers = {};
  config.headers["Accept"] = "application/json";
  config.headers["Authorization"] = window.localStorage.getItem("token")
    ? "Bearer " + window.localStorage.getItem("token")
    : "";
  config.headers["Content-Type"] = "application/json";
  return config;
};

export const applyHeadersRequestInterceptor = (axios) => {
  axios.interceptors.request.use(headersRequestInterceptor);
  return axios;
};
