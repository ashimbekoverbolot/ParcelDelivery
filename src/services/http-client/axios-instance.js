import axios from "axios";

import { applyHeadersRequestInterceptor } from "./headers-request-interceptor";

const instance = axios.create({
  baseURL: "https://909-logistic.kz/api/",
  timeout: 10 * 1000,
});

applyHeadersRequestInterceptor(instance);

export default instance;
