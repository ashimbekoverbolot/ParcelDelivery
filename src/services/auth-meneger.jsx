import { nanoid } from "nanoid";

import { localStorage } from "@/services/storage";

export const isAuthenticated = () => {
  return localStorage.getItem("AUTH");
};

export const setIsAuthenticated = (value) => {
  if (typeof value === "boolean") {
    localStorage.setItem("AUTH", value);
  }
  return value;
};

export const setILogin = (value) => {
  if (typeof value === "string") {
    localStorage.setItem("LOGIN", value);
  }
};

export const getILogin = () => {
  return localStorage.getItem("LOGIN");
};

export const removeILogin = () => {
  localStorage.removeItem("LOGIN");
};

export const fallbackDeviceId = () => {
  const deviceId = nanoid(12);
  localStorage.setItem("device_id", deviceId);
  return deviceId;
};

export const getDeviceId = () => {
  return localStorage.getItem("device_id");
};
