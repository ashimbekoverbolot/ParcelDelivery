import { Grid, Input, Typography } from "@mui/material";

export const UserInfo = (props) => {
  const { icon, title, subtitle, isEditing, onChange, type, mb = true } = props;

  return (
    <Grid container alignItems="center" mb={mb ? 2 : 0}>
      {icon && (
        <Grid item md={1} marginRight={2}>
          <img src={icon} alt="icon" />
        </Grid>
      )}
      <Grid>
        <Typography sx={{ fonSize: 16, fontWeight: 600, lineHeight: "28px" }}>
          {title}
        </Typography>
        {isEditing ? (
          <Input
            value={subtitle}
            onChange={(event) => {
              onChange(event.target.value);
            }}
            type={type}
            sx={{ width: "100%" }}
          />
        ) : (
          <Typography
            sx={{
              fonSize: 14,
              fontWeight: 400,
              lineHeight: "28px",
              color: "#0000ff",
            }}
          >
            {subtitle}
          </Typography>
        )}
      </Grid>
    </Grid>
  );
};
