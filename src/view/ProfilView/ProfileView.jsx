import ButtonBase from "../../components/Button/ButtonBase";
import Header from "../../components/Header/Header";

import classes from "./Profile.module.scss";

import { useEffect, useState } from "react";

import styled from "@emotion/styled";
import LogoutIcon from "@mui/icons-material/Logout";
import {
  Button,
  Grid,
  Input,
  Modal,
  Pagination,
  Typography,
} from "@mui/material";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import TabPanel from "../../components/TabPanel/TabPanel";

import { UserInfo } from "./components/UserInfo";

import { useNavigate } from "react-router";
import UserAPI from "../../api/user-api";
import { ReactComponent as CloseSvg } from "../../assets/icons/close.svg";
import LocationSvg from "../../assets/icons/location.svg";
import PersonSvg from "../../assets/icons/person.svg";
import PhoneSvg from "../../assets/icons/phone.svg";
import TrackCodeList from "../../components/TrackCodeList/TrackCodeList";
import { useUser } from "../../context/UserProvider";

const StyledLogOutButton = styled(ButtonBase)({
  padding: 14,
  minWidth: 40,
  ".MuiButton-startIcon": {
    margin: 0,
  },
});

const StyledTab = styled(Tab)({
  fontWeight: 600,
  fontSize: "18px",
  lineHeight: "28px",
  color: "#101828",
});

const StyledTabs = styled(Tabs)({
  ".MuiTabs-flexContainer": {
    paddingLeft: 30,
  },
  ".MuiTabs-indicator": {
    backgroundColor: "#0000ff",
    height: 3,
  },
});

const StyledUserInfoContainer = styled("div")({
  border: "1px solid #0000ff",
  borderRadius: 5,
});

const StyledModalTitle = styled(Typography)({
  fontWeight: 600,
  fontSize: "18px",
  lineHeight: "28px",
  color: "#000000",
  marginBottom: 18,
});

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "#FFFFFF",
  borderRadius: "18px",
  boxShadow: 24,
  p: 4,
};

const StyledInput = styled(Input)(() => ({
  width: "100%",
  marginBottom: "25px",
  ".MuiInput-input": {
    width: "100%",
    height: 38,
    border: "1px solid #c9c9c9",
    borderRadius: 5,
    padding: "0 5px",
  },

  "&:before": {
    borderBottom: "5px solid transparent",
    borderRadius: "0 0 3px 3px",
  },

  "&:hover:not(.Mui-disabled, .Mui-error):before": {
    borderBottom: "5px solid #FE5D03",
    borderRadius: "0 0 3px 3px",
  },

  "&:after": {
    borderBottom: "5px solid #FE5D03",
    borderRadius: "0 0 3px 3px",
  },
}));

const StyledCloseButton = styled(Button)({
  position: "absolute",
  top: "20px",
  right: "20px",
  cursor: "pointer",
  borderRadius: "50%",
  padding: 0,
  minWidth: 0,
  color: "#0000ff",
});

const ProfileView = () => {
  const { userData, setUserData } = useUser();
  const [value, setValue] = useState(0);
  const [open, setOpen] = useState(false);

  const [name, setName] = useState(userData.first_name);
  const [phone, setPhone] = useState(userData.phone_number);
  const [location, setLocation] = useState(userData.location);
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [repeatNewPassword, setRepeatNewPassword] = useState("");
  const [error, setError] = useState("");

  const [totalPage, setTotalPage] = useState(0);
  const [trackCode, setTrackCode] = useState("");
  const [productName, setProductName] = useState("");
  const [trackCodeData, setTrackCodeData] = useState("");

  const [isEditing, setIsEditing] = useState(false);
  const [isPasswordEditing, setIsPasswordEditing] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const navigate = useNavigate();
  const handleLogOut = () => {
    window.localStorage.removeItem("token");
    return navigate("/");
  };

  const getAllPosts = async (page) => {
    const data = await UserAPI.getPosts(page);

    setTotalPage(data.max_page_size);
    setTrackCodeData(data.data);
    return data.data;
  };

  useEffect(() => {
    if (!window.localStorage.getItem("token")) {
      return navigate("/");
    }

    const getUserData = async () => {
      const { data } = await UserAPI.getUserData();

      if (data?.user_code) {
        setUserData(data);
        setName(data.first_name);
        setPhone(data.phone_number);
        setLocation(data.location);
      }
    };
    getUserData();

    getAllPosts();
  }, [userData.user_code, navigate, setUserData]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleSave = async () => {
    const { data } = await UserAPI.saveUserData({
      name,
      phoneNumber: phone,
      location,
    });

    setUserData(data);
    setIsEditing(false);
    setIsPasswordEditing(false);
  };

  const hadleSavePassword = async () => {
    if (newPassword !== repeatNewPassword) {
      setError("Пароль не совпадает");
      return;
    }

    try {
      const { data } = await UserAPI.updatePassword({
        userCode: userData.user_code,
        oldPassword,
        newPassword,
      });
      if (data) {
        setError("");
        setOldPassword("");
        setNewPassword("");
        setRepeatNewPassword("");
        setIsPasswordEditing(false);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleAddTrackCode = async () => {
    const { data } = await UserAPI.addTrackCode({
      trackCode: String(trackCode),
      title: productName,
      userId: userData.id,
    });

    if (data?.id) {
      getAllPosts();
      handleClose();
    }
  };

  const handleChangePagination = async (_, page) => {
    const data = await getAllPosts(page);
    setTrackCodeData(data);
  };

  return (
    <>
      <div className="container">
        <Header>
          <p className={classes.logOutText}>Выйти</p>
          <StyledLogOutButton
            startIcon={<LogoutIcon />}
            onClick={handleLogOut}
          />
        </Header>
      </div>
      <Box sx={{ width: "100%", overflow: "hidden" }}>
        <Box sx={{ borderBottom: 3, borderColor: "#0000ff" }}>
          <StyledTabs
            value={value}
            onChange={handleChange}
            textColor="primary"
            indicatorColor="primary"
            aria-label="basic tabs example"
            sx={{ position: "relative", bottom: "-3px" }}
          >
            <StyledTab label="Профиль" />
            <StyledTab label="Посылки" />
          </StyledTabs>
        </Box>
        <TabPanel value={value} index={0} style={{ padding: "30px" }}>
          <Grid container>
            <Grid item xs={12} sm={7} md={8} lg={6} xl={5}>
              <StyledUserInfoContainer>
                <Grid container m={2}>
                  {!isPasswordEditing && (
                    <Grid item xs={12} md={7}>
                      <UserInfo
                        icon={PersonSvg}
                        title="ФИО"
                        subtitle={name}
                        isEditing={isEditing}
                        onChange={(value) => {
                          setName(value);
                        }}
                      />
                      <UserInfo
                        icon={PhoneSvg}
                        title="Номер телефона"
                        subtitle={phone}
                        isEditing={isEditing}
                        onChange={(value) => {
                          setPhone(value);
                        }}
                      />

                      <UserInfo
                        icon={LocationSvg}
                        title="Город"
                        subtitle={location}
                        isEditing={isEditing}
                        onChange={(value) => {
                          setLocation(value);
                        }}
                      />
                    </Grid>
                  )}
                  {isPasswordEditing && (
                    <Grid item xs={12} md={7}>
                      <UserInfo
                        title="Старый пароль"
                        type={"password"}
                        subtitle={oldPassword}
                        isEditing={isPasswordEditing}
                        onChange={(value) => {
                          setOldPassword(value);
                        }}
                      />
                      <UserInfo
                        title="Новый пароль"
                        subtitle={newPassword}
                        type={"password"}
                        isEditing={isPasswordEditing}
                        onChange={(value) => {
                          setNewPassword(value);
                        }}
                      />

                      <UserInfo
                        title="Повторите новый пароль"
                        subtitle={repeatNewPassword}
                        type={"password"}
                        isEditing={isPasswordEditing}
                        onChange={(value) => {
                          setRepeatNewPassword(value);
                        }}
                      />
                      {error && <p style={{ color: "red" }}>{error}</p>}
                    </Grid>
                  )}
                  <Grid item xs={5} sm={4} alignSelf="end" mb={2}>
                    <ButtonBase
                      onClick={() => {
                        if (isPasswordEditing) {
                          hadleSavePassword();
                        } else {
                          setIsEditing(false);
                          setIsPasswordEditing(true);
                        }
                      }}
                      sx={{
                        marginBottom: 25,
                        width: "200px",
                      }}
                    >
                      {isPasswordEditing ? "Сохранить" : "Изменить пароль "}
                    </ButtonBase>
                    <ButtonBase
                      onClick={() => {
                        if (isEditing) {
                          handleSave();
                        } else {
                          setIsPasswordEditing(false);
                          setIsEditing(true);
                        }
                      }}
                      sx={{
                        width: "200px",
                      }}
                    >
                      {isEditing ? "Сохранить" : "Редактировать"}
                    </ButtonBase>
                  </Grid>
                </Grid>
              </StyledUserInfoContainer>
            </Grid>
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={1} className={classes.tabPanel}>
          <ButtonBase onClick={handleOpen} className={classes.addbtn}>Добавить посылку +</ButtonBase>
          <TrackCodeList data={trackCodeData} user />
          {totalPage > 1 && (
            <div
              style={{
                display: "flex",
                width: "100%",
                justifyContent: "center",
              }}
            >
              <Pagination
                sx={{ margin: "15px 0" }}
                count={totalPage}
                color="secondary"
                size="large"
                onChange={(_, page) => {
                  handleChangePagination(_, page);
                }}
              />
            </div>
          )}
        </TabPanel>
      </Box>

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Grid
            item
            width="100%"
            display="flex"
            alignItems="center"
            flexDirection="column"
          >
            <StyledCloseButton onClick={handleClose}>
              <CloseSvg />
            </StyledCloseButton>
            <StyledModalTitle
              id="modal-modal-title"
              variant="h6"
              component="h2"
              textAlign="center"
            >
              Вставьте трек-код
            </StyledModalTitle>
            <StyledInput
              onChange={(event) => {
                setTrackCode(event.target.value);
              }}
            />
            <StyledModalTitle
              id="modal-modal-title"
              variant="h6"
              component="h2"
              textAlign="center"
            >
              Название товара
            </StyledModalTitle>
            <StyledInput
              onChange={(event) => {
                setProductName(event.target.value);
              }}
            />

            <ButtonBase onClick={handleAddTrackCode}>Сохранить</ButtonBase>
          </Grid>
        </Box>
      </Modal>
    </>
  );
};

export default ProfileView;
