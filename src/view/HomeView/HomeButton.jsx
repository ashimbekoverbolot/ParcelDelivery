import styled from "@emotion/styled";
import { Link } from "@mui/material";

const StyledHomeButton = styled(Link)({
  textDecoration: "none",
  color: "#FFFFFF",
  borderRadius: "15px",
  background: "#0000ff",
  width: "350px",
  height: "50px",
  textAlign: "center",
  alignItems: "center",
});

export const HomeButton = (props) => {
  const { href = "#", children, ...rest } = props;
  return (
    <StyledHomeButton href={href} {...rest}>
      {children}
    </StyledHomeButton>
  );
};
