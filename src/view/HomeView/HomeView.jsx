import styled from "@emotion/styled";

import BackgroundImage from "../../assets/background.png";
import LogoSm from "../../assets/logo-sm.png";
import Logo from "../../assets/logo.png";
import { HomeButton } from "./HomeButton";

import { Typography } from "@mui/material";
import { useEffect } from "react";
import { ReactComponent as LocationMarkSvg } from "../../assets/icons/locationMark.svg";

const StyledContainer = styled("div")({
  maxWidth: "100vw",

  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  padding: "15px",
  margin: "0 auto",
});

const StyledLinksWrapper = styled("div")({
  maxWidth: "600px",
  width: "100%",
  marginBottom: "30px",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
});

const StyledIconWrapper = styled("div")({
  marginRight: "15px",
  verticalAlign: "middle",
  svg: {
    verticalAlign: "middle",
  },
});

const StyledAddressContainer = styled("div")({
  borderRadius: " 15px",
  background: "#FFF",
  padding: "27px 0",
  maxWidth: "445px",
  width: "100%",
  textAlign: "center",
  marginBottom: "34px",
  marginTop: "34px",
});

const StyledFooter = styled("div")({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  marginBottom: "34px",
});

const HomeView = () => {
  useEffect(() => {
    const styles = {
      background: `url(${BackgroundImage})`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      backgroundPosition: "center",
      height: window.innerHeight > 1200 ? "100vh" : "100%",
    };

    Object.assign(document.body.style, styles);
  });
  return (
    <StyledContainer>
      <img src={window.innerWidth > 560 ? Logo : LogoSm} alt="logo" />
      <StyledLinksWrapper>
        <HomeButton href="/login">ОТСЛЕЖИВАНИЕ ПОСЫЛОК ИЗ КИТАЯ</HomeButton>
        <StyledAddressContainer>
          <Typography fontSize={26} fontWeight={600} marginBottom={"15px"}>
            КАРГО
          </Typography>
          <Typography fontSize={22} fontWeight={500} marginBottom={"10px"}>
            КИТАЙ-КАЗАХСТАН
          </Typography>
          <Typography fontSize={20} fontWeight={400} marginBottom={"10px"}>
            Срок доставки 5-15 дней
          </Typography>
          <Typography fontSize={20} fontWeight={400} marginBottom={"15px"}>
            Цена за кг - 4.5$
          </Typography>
        </StyledAddressContainer>
      </StyledLinksWrapper>
      <StyledFooter>
        <Typography
          fontSize={20}
          fontWeight={500}
          marginBottom={"34px"}
          display={"flex"}
          alignItems={"center"}
        >
          <StyledIconWrapper>
            <LocationMarkSvg />
          </StyledIconWrapper>
          г. Алматы, Проспект Райымбека 225и
        </Typography>
        <Typography fontSize={20} fontWeight={500}>
          Наш адрес в Китае: 地址：广东省佛山市南海区
          里水镇洲村大管家仓储园B1栋29号库房
        </Typography>
      </StyledFooter>
    </StyledContainer>
  );
};

export default HomeView;
