import { useNavigate } from "react-router-dom";

import { useState } from "react";
import UserAPI from "../../api/user-api";
import AuthHeader from "../../components/Auth/AuthHeader";
import Input from "../../components/Input/Input";
import { useUser } from "../../context/UserProvider";
import classes from "./SignUpView.module.scss";

const SignUpView = () => {
  const navigate = useNavigate();
  const [code, setCode] = useState("");
  const [password, setPassword] = useState("");
  const [cofrimPassword, setConfirmPassword] = useState("");
  const [loading, setLoading] = useState("");
  const [error, setError] = useState("");

  const { setUserData } = useUser();

  const handleClick = async () => {
    if (code.length > 3 && password.length > 5 && password === cofrimPassword) {
      setLoading(true);
      try {
        const { data } = await UserAPI.signUp({
          clientCode: String(code),
          password: String(password),
        });
        setUserData(data);

        window.localStorage.setItem("token", data?.token);
        if (data.user_code) {
          setLoading(false);
          navigate("/profile");
        }
      } catch ({ response }) {
        setError(response.data.data.message);
      }
    }
  };

  return (
    <div className={classes.container}>
      <AuthHeader
        signUp={{ title: "Авторизация", href: "/login" }}
        title={"Регистрация"}
        submitButton={{
          onClick: handleClick,
          text: "Продолжить",
          type: "button",
        }}
        loading={loading}
      >
        <Input
          label="Код клиента: "
          onChange={(event) => setCode(event.target.value)}
        />
        <Input
          label="Пароль: "
          type="password"
          onChange={(event) => setPassword(event.target.value)}
        />
        <Input
          label="Подтвердите пароль: "
          type="password"
          onChange={(event) => setConfirmPassword(event.target.value)}
        />
        {error && <p className={classes.error}>{error}</p>}
      </AuthHeader>
    </div>
  );
};

export default SignUpView;
