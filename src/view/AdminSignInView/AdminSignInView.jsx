import { useNavigate } from "react-router";
import AuthHeader from "../../components/Auth/AuthHeader";
import Input from "../../components/Input/Input";

import { useState } from "react";
import UserAPI from "../../api/user-api";
import classes from "./AdminSignInView.module.scss";

function AdminSignInView() {
  const navigate = useNavigate();
  const [code, setCode] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState("");
  const [error, setError] = useState("");

  const handleSignIn = async () => {
    setLoading(true);
    try {
      const data = await UserAPI.signIn({ clientCode: code, password });
      if (data?.access) {
        window.localStorage.setItem("token", data.access);
        setLoading(false);
        return navigate("/admin/profile");
      }
    } catch ({ response }) {
      setError(response.data.detail);
      setLoading(false);
    }
  };
  return (
    <>
      <div className={classes.container}>
        <AuthHeader
          submitButton={{
            onClick: handleSignIn,
            text: "Войти",
            type: "button",
          }}
          loading={loading}
        >
          <Input
            label="Логин: "
            onChange={(event) => setCode(event.target.value)}
          />
          <Input
            label="Пароль: "
            type="password"
            onChange={(event) => setPassword(event.target.value)}
          />
          {error && <p className={classes.error}>{error}</p>}
        </AuthHeader>
      </div>
    </>
  );
}

export default AdminSignInView;
