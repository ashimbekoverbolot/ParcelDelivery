import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router";

import styled from "@emotion/styled";
import {
  Button,
  FormControl,
  Grid,
  Input,
  MenuItem,
  Modal,
  Pagination,
  Select,
  Typography,
} from "@mui/material";

import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import SearchIcon from "@mui/icons-material/Search";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";

import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import dayjs from "dayjs";
import UserAPI from "../../api/user-api";
import BarcodeImage from "../../assets/barcode.png";
import EmptyList from "../../assets/emptyList.png";
import { ReactComponent as CloseSvg } from "../../assets/icons/close.svg";
import { ReactComponent as FilterSvg } from "../../assets/icons/filter.svg";
import ButtonBase from "../../components/Button/ButtonBase";
import Header from "../../components/Header/Header";
import TabPanel from "../../components/TabPanel/TabPanel";
import TrackCodeList from "../../components/TrackCodeList/TrackCodeList";
import { useUser } from "../../context/UserProvider";
import { createVariants } from "../../utils/styled-utils";

const StyledAddButton = styled(ButtonBase)({
  padding: "6px 16px",
  marginLeft: "15px",
  minWidth: "145px",
});

const filterModalStyles = {
  position: "absolute",
  display: "flex",
  alignItems: "center",
  flexDirection: "column",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  border: "2px solid #0000ff",
  background: "#ffffff",
  padding: "20px",
};

const StyledTab = styled(Tab, {
  shouldForwardProp: (propName) => propName !== "variant",
})(
  {
    fontWeight: 500,
    fontSize: "14px",
    lineHeight: "28px",
    color: "#101828",
    border: "1px solid #0000ff",
    borderRadius: "5px",
    padding: "10px 16px",
    minHeight: 0,
    height: "40px",

    ".MuiTab-selected": {
      background: "red",
    },
  },
  createVariants("variant", {
    leftSide: () => ({
      borderRadius: "10px 0 0 10px",
    }),

    center: ({ theme }) => ({
      borderRadius: 0,
    }),

    rightSide: () => ({
      borderRadius: "0 10px 10px 0",
    }),
  })
);

const StyledButton = styled(Button)({
  display: "flex",
  alignItems: "center",
  background: "#FFFFFF",
  border: "1px solid #0000ff",
  color: "#0000ff",
  boxShadow: "0px 1px 2px rgba(16, 24, 40, 0.05)",
  borderRadius: "8px",
  padding: "6px 16px",
  margin: "0",
  width: "100%",
});

const StyledTabs = styled(Tabs)({
  display: "flex",

  ".MuiTabs-indicator": {
    backgroundColor: "transparent",
  },
});

const StyledModalTitle = styled(Typography)({
  fontWeight: 600,
  fontSize: "18px",
  lineHeight: "28px",
  color: "#000000",
  marginBottom: "10px",
  textAlign: "center",
});

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "#FFFFFF",
  borderRadius: "18px",
  boxShadow: 24,
  p: 4,
};

const fixedBarCodeStyles = {
  position: "fixed",
  right: "30px",
  bottom: "45px",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  bgcolor: "#FFFFFF",
  width: "fit-content",
  background: "#FFFFFF",
  boxShadow: "0px 0px 10px 1px rgba(127, 86, 217, 0.6)",
  borderRadius: "10px",
  p: 4,
};

const StyledFixedBarCodeImage = styled("img")({
  width: "80px",
  height: "35px",
});

const StyledSearchInput = styled(Input)(() => ({
  width: "100%",
  ".MuiInput-input": {
    width: "100%",
    height: 38,
    padding: "0 5px",
  },

  "&:before": {
    borderBottom: "none",
    borderRadius: "0 0 3px 3px",
  },

  "&:hover:not(.Mui-disabled, .Mui-error):before": {
    borderBottom: "none",
    borderRadius: "0 0 3px 3px",
  },

  "&:after": {
    borderBottom: "none",
    borderRadius: "0 0 3px 3px",
  },
}));

const StyledCloseButton = styled(Button)({
  position: "absolute",
  top: "20px",
  right: "20px",
  cursor: "pointer",
  borderRadius: "50%",
  padding: 0,
  minWidth: 0,
  color: "#0000ff",
});

const StyledBarCodeImage = styled("img")({
  width: "185px",
  height: "80px",
});

const StyledSelect = styled(Select)({
  minWidth: "145px",
  width: "100%",
  height: 40,

  "& .MuiSelect-outlined": {
    padding: "5px 10px",
    width: "100%",
  },
});

const StyledContainerForEmptyList = styled("div")({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",

  img: {
    marginTop: 70,
    width: 300,
  },

  p: {
    fontSize: 40,
    fontWeight: 600,
  },
});

const notAcceptableKeyCodes = [
  9, 13, 16, 17, 18, 20, 27, 46, 91, 32, 93, 220, 8, 112, 113, 114, 115, 116,
  117, 118, 119, 120, 121, 122, 123,
];

const transformData = (data) => {
  let newData = [];
  for (let index = 0; index < data?.length; index++) {
    newData.push({ ...data[index], isSelected: false });
  }

  return newData;
};

const isTackCodeSelected = (data, trackCode, selectAll) => {
  data.forEach((track) => {
    if (track.track_code === trackCode) {
      track.isSelected = track.isSelected ? false : true;
    }
  });
};

const AdminProfileView = () => {
  const navigate = useNavigate();

  const { userData, setUserData } = useUser();
  const [value, setValue] = useState(0);
  const [totalPage, setTotalPage] = useState(0);
  const [statusValue, setStatusValue] = useState("");
  const [open, setOpen] = useState(false);
  const [isOpenFilterModal, setIsOpenFilterModal] = useState(false);
  const [from, setFrom] = useState(dayjs());
  const [to, setTo] = useState(dayjs());
  const [isDataFetched, setIsDataFetched] = useState(false);

  const [collectedTrackCodes, setCollectedTrackCodes] = useState([]);

  const [loading, setLoading] = useState(false);
  const [trackCodeData, _setTrackCodeData] = useState("");

  const setTrackCodeData = (data) => {
    _setTrackCodeData(transformData(data));
  };

  const { status } = useParams();

  let word = "";
  let lastAddedTrackCode = "";

  const getAllPosts = async (page) => {
    const data = await UserAPI.getPosts(page);

    setTotalPage(data.max_page_size);
    return data.data;
  };

  const handleKeydown = (event) => {
    if (!notAcceptableKeyCodes.includes(event.keyCode)) {
      word = word + event.key;
    }

    if (
      event.keyCode === 13 &&
      word.length > 0 &&
      lastAddedTrackCode !== word
    ) {
      handleUpdate(word);
    }
  };

  useEffect(() => {
    if (!window.localStorage.getItem("token") && !userData.is_admin) {
      return navigate("/admin");
    }

    setLoading(true);
    if (!userData.user_code) {
      const getUserData = async () => {
        try {
          const { data } = await UserAPI.getUserData();
          if (data.is_admin) {
            setUserData(data);
          }
        } catch (error) {
          navigate("/admin");
        }
      };
      getUserData();
    }

    const getPosts = async () => {
      const data = await getAllPosts();
      setTrackCodeData(transformData(data));
      setLoading(false);
      setIsDataFetched(true);
    };

    getPosts();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (status) {
      window.addEventListener("keydown", handleKeydown);
    }

    return () => {
      window.removeEventListener("keydown", handleKeydown);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status]);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const handleUpdate = async (trackCode) => {
    const { data } = await UserAPI.updateTrackStatus({
      trackCode: String(trackCode),
      status: status,
    });

    if (data.id) {
      const data = await getAllPosts();
      setTrackCodeData(data);
      word = "";
    }
  };

  const handleCollectTrackCodes = ({ trackCode = "", selectAll = null }) => {
    isTackCodeSelected(trackCodeData, trackCode, selectAll);

    setCollectedTrackCodes((prev) => {
      if (prev.indexOf(trackCode) !== -1) {
        return prev.filter((code) => {
          return code !== trackCode;
        });
      }

      return [...prev, trackCode];
    });
  };

  const handleChange = async (event, newValue) => {
    setLoading(true);

    setValue(newValue);
    if (newValue === 0) {
      const data = await getAllPosts();
      setTrackCodeData(data);
    } else if (newValue === 1) {
      const data = await getAllPosts();
      const filteredData = data.filter((track) => track.status === "SNT");
      setTrackCodeData(filteredData);
    } else if (newValue === 2) {
      const data = await getAllPosts();
      const filteredData = data.filter((track) => track.status === "DLV");
      setTrackCodeData(filteredData);
    }
    setIsDataFetched(true);
    setLoading(false);
  };

  const handleDelete = async () => {
    setLoading(true);
    await UserAPI.deleteTrackCode(collectedTrackCodes);
    const data = await getAllPosts();
    setTrackCodeData(data);
    setLoading(false);
  };

  const handleSelectedChange = async (event) => {
    setStatusValue(event.target.value);

    if (collectedTrackCodes.length <= 0) {
      return;
    }

    setLoading(true);
    collectedTrackCodes.forEach(async (trackCode) => {
      await UserAPI.updateTrackStatus({
        trackCode: String(trackCode),
        status: event.target.value,
      });
    });

    if (value === 0) {
      const data = await getAllPosts();
      setTrackCodeData(data);
    } else if (value === 1) {
      const data = await getAllPosts();
      const filteredData = data.filter((track) => track.status === "SNT");
      setTrackCodeData(filteredData);
    } else if (value === 2) {
      const data = await getAllPosts();
      const filteredData = data.filter((track) => track.status === "DLV");
      setTrackCodeData(filteredData);
    }
    setCollectedTrackCodes([]);
    setLoading(false);
  };

  let timer = "";

  const handleSearchInputChange = (event) => {
    if (timer) {
      clearInterval(timer);
    }

    timer = setTimeout(async () => {
      setLoading(true);
      const { data } = await UserAPI.searchPosts(event.target.value);
      setLoading(false);
      setTrackCodeData(data);
    }, 1000);
  };

  const handleOpenFilterModal = () => {
    setIsOpenFilterModal(true);
  };
  const handleCloeFilterModal = () => {
    setIsOpenFilterModal(false);
  };

  const handleApplyFilterData = async () => {
    const data = await UserAPI.filteredPosts({
      from: `${dayjs(from).$y}-${dayjs(from).$M + 1}-${dayjs(from).$D}`,
      to: `${dayjs(to).$y}-${dayjs(to).$M + 1}-${dayjs(to).$D}`,
    });

    setTrackCodeData(data.data);
    handleCloeFilterModal();
  };

  const handleChangePagination = async (_, page) => {
    const data = await getAllPosts(page);
    setTrackCodeData(data);
  };

  return (
    <>
      <div className="container">
        <Header>
          {collectedTrackCodes.length > 0 && (
            <>
              <Button onClick={handleDelete}>
                <DeleteForeverIcon />
              </Button>
              <Box sx={{ minWidth: 110 }}>
                <FormControl fullWidth>
                  <StyledSelect
                    id="select"
                    value={statusValue || '"Выберите статус"'}
                    onChange={handleSelectedChange}
                  >
                    <MenuItem value={"NST"}>Нет статуса</MenuItem>
                    <MenuItem value={"SCH"}>На складе в Китае</MenuItem>
                    <MenuItem value={"SKA"}>На складе в Казахстане</MenuItem>
                    <MenuItem value={"SNT"}>Отправлено</MenuItem>
                    <MenuItem value={"DLV"}>Доставлено</MenuItem>
                  </StyledSelect>
                </FormControl>
              </Box>
            </>
          )}
          <StyledAddButton onClick={handleOpen}>+ Добавить</StyledAddButton>
        </Header>
      </div>
      <Box
        sx={{
          padding: "0 30px 0",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Grid container justifyContent="space-between">
          <Grid item xs={6}>
            <StyledTabs value={value} onChange={handleChange}>
              <StyledTab label="Все" variant="leftSide" />
              <StyledTab label="Отправлено" variant="center" />
              <StyledTab label="Доставлено" variant="rightSide" />
            </StyledTabs>
          </Grid>
          <Grid item xs={6}>
            <Grid container justifyContent="flex-end">
              <Grid item xs={6} mr={3}>
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "flex-end",
                    background: "#FFFFFF",
                    border: "1px solid #0000ff",
                    boxShadow: "0px 1px 2px rgba(16, 24, 40, 0.05)",
                    borderRadius: "8px",
                    padding: "0 10px",
                  }}
                >
                  <SearchIcon sx={{ color: "action.active", mr: 1, my: 0.5 }} />
                  <StyledSearchInput
                    id="input-with-sx"
                    variant="standard"
                    placeholder="Поиск"
                    onChange={handleSearchInputChange}
                  />
                </Box>
              </Grid>
              <Grid item xs={3} justifyContent="flex-end">
                <StyledButton
                  startIcon={<FilterSvg />}
                  onClick={handleOpenFilterModal}
                >
                  Фильтры
                </StyledButton>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        {trackCodeData.length <= 0 && isDataFetched ? (
          <>
            <StyledContainerForEmptyList>
              <img src={EmptyList} alt="" />
              <Typography>К сожалению ничего не найдена</Typography>
            </StyledContainerForEmptyList>
          </>
        ) : (
          <>
            <TabPanel value={value} index={0}>
              <TrackCodeList
                data={trackCodeData}
                isAdmin={userData.is_admin}
                loading={loading}
                onCollect={handleCollectTrackCodes}
              />
            </TabPanel>
            <TabPanel value={value} index={1}>
              <TrackCodeList
                data={trackCodeData}
                isAdmin={userData.is_admin}
                loading={loading}
                onCollect={handleCollectTrackCodes}
              />
            </TabPanel>
            <TabPanel value={value} index={2}>
              <TrackCodeList
                data={trackCodeData}
                isAdmin={userData.is_admin}
                loading={loading}
                onCollect={handleCollectTrackCodes}
              />
            </TabPanel>
            {totalPage > 1 && (
              <Pagination
                sx={{ margin: "15px 0" }}
                count={totalPage}
                color="secondary"
                size="large"
                onChange={(_, page) => {
                  handleChangePagination(_, page);
                }}
              />
            )}
          </>
        )}
      </Box>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Grid
            item
            width="100%"
            display="flex"
            alignItems="center"
            flexDirection="column"
          >
            <StyledCloseButton onClick={handleClose}>
              <CloseSvg />
            </StyledCloseButton>
            <StyledBarCodeImage src={BarcodeImage} alt="barcode" />
            <StyledModalTitle
              id="modal-modal-title"
              variant="h6"
              component="h2"
            >
              Вы в режиме сканирования
            </StyledModalTitle>
            <Typography
              sx={{
                fontSize: 16,
                textAlign: "center",
                color: "#0000ff",
                marginBottom: "30px",
              }}
            >
              Выберите режим сканирования
            </Typography>
            <Grid>
              <ButtonBase
                sx={{ marginRight: "20px" }}
                href={"/admin/stock/SKA"}
              >
                Склад Казахстанa
              </ButtonBase>
              <ButtonBase href={"/admin/stock/SCH"}>Склад Китая</ButtonBase>
            </Grid>
          </Grid>
        </Box>
      </Modal>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <Modal open={isOpenFilterModal} onClose={handleCloeFilterModal}>
          <Box style={filterModalStyles}>
            <Typography variant="h4" mb={4}>
              Выберите дату для фильтра{" "}
            </Typography>
            <Grid
              container
              alignItems={"center"}
              justifyContent={"center"}
              mb={4}
            >
              <Grid item xs={5} ml={2}>
                <DatePicker
                  defaultValue={from}
                  onChange={(newValue) => setFrom(newValue)}
                />
              </Grid>
              <Grid item xs={5}>
                <DatePicker
                  defaultValue={to}
                  onChange={(newValue) => {
                    setTo(newValue);
                  }}
                />
              </Grid>
            </Grid>
            <Grid container alignItems={"center"} justifyContent={"center"}>
              <Grid item xs={4} mr={4}>
                <StyledAddButton
                  sx={{ width: "100%" }}
                  onClick={handleApplyFilterData}
                >
                  Применить
                </StyledAddButton>
              </Grid>
              <Grid item xs={4}>
                <StyledButton
                  sx={{ height: "100%", padding: "10px 24px" }}
                  onClick={handleCloeFilterModal}
                >
                  Отмена
                </StyledButton>
              </Grid>
            </Grid>
          </Box>
        </Modal>
      </LocalizationProvider>
      {status && (
        <Box sx={fixedBarCodeStyles}>
          <Grid
            item
            width="100%"
            display="flex"
            alignItems="center"
            flexDirection="column"
          >
            <StyledFixedBarCodeImage src={BarcodeImage} alt="barcode" />
            <Typography
              id="modal-modal-title"
              variant="h6"
              component="h2"
              sx={{ fontSize: "14px", fontWeight: 600 }}
            >
              Вы в режиме сканирования
            </Typography>
            <Typography
              sx={{
                fontSize: 12,
                textAlign: "center",
                color: "#0000ff",
                marginBottom: "30px",
              }}
            >
              Выберите режим сканирования
            </Typography>
            <ButtonBase
              sx={{ fontWeight: 500, fontSize: "8px" }}
              href={"/admin/profile"}
            >
              Выйти из режима сканирования
            </ButtonBase>
          </Grid>
        </Box>
      )}
    </>
  );
};

export default AdminProfileView;
