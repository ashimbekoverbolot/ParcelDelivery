import axios from "../services/http-client/axios-instance";

class UserAPI {
  static signUp = async ({ clientCode, password }) => {
    const response = await axios.post(
      "users/register/",
      JSON.stringify({ user_code: clientCode, password }),
      {
        headers: {
          Authorization: "",
        },
      }
    );

    return response.data;
  };

  static signIn = async ({ clientCode, password }) => {
    const response = await axios.post(
      "/auth/jwt/create/",
      JSON.stringify({ user_code: clientCode, password })
    );

    return response.data;
  };

  static getUserData = async () => {
    const response = await axios.get("users/profile/");

    return response.data;
  };

  static saveUserData = async ({ name, phoneNumber, location }) => {
    const response = await axios.patch(
      "/users/profile/update/",
      JSON.stringify({
        first_name: name,
        phone_number: phoneNumber,
        location,
      })
    );

    return response.data;
  };

  static addTrackCode = async ({ trackCode, userId, title }) => {
    const response = await axios.post(
      "pos/create/",
      JSON.stringify({
        track_code: trackCode,
        user_id: userId,
        title: title,
        status: "NST",
      })
    );

    return response.data;
  };

  static deleteTrackCode = async (trackCode) => {
    const response = await axios.post("pos/delete/", {
      track_code: trackCode,
    });

    return response.data;
  };

  static updateTrackStatus = async ({ trackCode, status = "NST" }) => {
    const response = await axios.patch(
      "pos/update/",
      JSON.stringify({
        track_code: trackCode,
        status: status,
      })
    );

    return response.data;
  };

  static updateTrackCodeStatus = async ({ trackCode, newTrackCode, title }) => {
    const response = await axios.patch(
      "pos/update/",
      JSON.stringify({
        track_code: trackCode,
        title,
        new_track_code: newTrackCode,
      })
    );

    return response.data;
  };

  static updatePassword = async ({ userCode, oldPassword, newPassword }) => {
    const response = await axios.patch(
      "users/update/password/",
      JSON.stringify({
        user_code: userCode,
        old_password: oldPassword,
        new_password: newPassword,
      })
    );

    return response.data;
  };

  static getPosts = async (page = 1) => {
    const response = await axios.get(`pos/?page=${page}`);

    return response.data;
  };

  static filteredPosts = async ({ from, to }) => {
    const response = await axios.get(
      `pos/china/period?from_time=${from}&end_time=${to}`
    );

    return response.data;
  };
  static getUpdatePost = async () => {
    const response = await axios.get("pos/update/2/");

    return response.data;
  };

  static searchPosts = async (value) => {
    const response = await axios.get(`pos?search=${value}`);

    return response.data;
  };
}

export default UserAPI;
