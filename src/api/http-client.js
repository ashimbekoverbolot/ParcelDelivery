import axios from 'axios';
import queryString from 'query-string';

import { applyExtractorResponseInterceptor } from '@/api/interceptors/extractor-response-interceptor';
import { applyHeadersRequestInterceptor } from '@/api/interceptors/headers-request-interceptor';
import { applyUnauthenticatedResponseInterceptor } from '@/api/interceptors/unauthorized-response-interceptor';

export const httpClient = axios.create({
  baseURL: process.env.NEXT_PUBLIC_ITV_API_BASE_URL,
  timeout: 10 * 1000,
  paramsSerializer: params => queryString.stringify(params, { arrayFormat: 'bracket' }),
});

applyHeadersRequestInterceptor(httpClient);
applyUnauthenticatedResponseInterceptor(httpClient);

// Should apply after all
applyExtractorResponseInterceptor(httpClient);
