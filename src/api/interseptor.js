import { serverHttpCodes } from "@/app.config";

export const applyUnauthorizedInterceptor = (axios, handler) => {
  axios.interceptors.response.use(
    (response) => response,
    (error) => {
      if (error.response?.status === serverHttpCodes.unauthorized) {
        handler();
      }
      return Promise.reject(error);
    }
  );
};
