import AuthHeader from "./components/Auth/AuthHeader";
import Input from "./components/Input/Input";

function App() {
  return (
    <>
      <AuthHeader>
        <Input label="Логин: " />
        <Input label="Пароль: " type="password" />
      </AuthHeader>
    </>
  );
}

export default App;
