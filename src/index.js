import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import SignInView from "./view/SignInView";
import SignUpView from "./view/SignUpView/SignUpView";

import { ThemeProvider } from "@mui/material";
import UserProvider from "./context/UserProvider";
import "./styles/global.scss";
import { theme } from "./styles/theme";
import AdminProfileView from "./view/AdminProfileView/AdminProfileView";
import AdminSignInView from "./view/AdminSignInView/AdminSignInView";
import HomeView from "./view/HomeView/HomeView";
import ProfileView from "./view/ProfilView/ProfileView";

const router = createBrowserRouter([
  {
    path: "/",
    element: <HomeView />,
  },
  {
    path: "/login",
    element: <SignInView />,
  },
  {
    path: "/admin/login",
    element: <AdminSignInView />,
  },
  {
    path: "/admin/profile",
    element: <AdminProfileView />,
  },
  {
    path: "/admin/stock/:status",
    element: <AdminProfileView />,
  },
  {
    path: "/signup",
    element: <SignUpView />,
  },
  {
    path: "/profile",
    element: <ProfileView />,
  },
]);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <UserProvider>
        <RouterProvider router={router} />
      </UserProvider>
    </ThemeProvider>
  </React.StrictMode>
);
