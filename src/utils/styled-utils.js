/**
 * @template T
 * @typedef {{ [key: string]: T }} Dictionary
 */

/**
 * @param {string} variantPropName
 * @param {Dictionary<Dictionary<string> | ((props: any) => Dictionary<string>)>} variants
 * @return {(props: Object) => Object}
 */
export const createVariants = (variantPropName, variants) => {
  return (props) => {
    const variant = props[variantPropName];
    return typeof variants[variant] === "function"
      ? variants[variant](props)
      : variants[variant];
  };
};
