import { Grid } from "@mui/material";
import classes from "./Header.module.scss";

import { ReactComponent as LogoSvg } from "../../assets/icons/logo.svg";

const Header = (props) => {
  return (
    <Grid container className={classes.container}>
      <Grid item xs={6} alignItems={"center"}>
        <LogoSvg className={classes.logo} />
      </Grid>
      <Grid
        item
        xs={5}
        className={classes.actionSide}
        justifyContent={"flex-end"}
      >
        {props.children}
      </Grid>
    </Grid>
  );
};

export default Header;
