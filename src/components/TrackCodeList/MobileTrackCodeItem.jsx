import styled from "@emotion/styled";
import { Box, Button, Grid } from "@mui/material";
import { ReactComponent as DoneSvg } from "../../assets/icons/done.svg";
import { ReactComponent as DownSvg } from "../../assets/icons/down.svg";
import { statuses } from "../../constants/statuses";
import classes from "./TrackCodeList.module.scss";

const StyledContainer = styled("div")({
  border: "1px solid #0000ff",
  borderRadius: 5,
  padding: "2px 15px",
  width: "fit-content",
});

export const MobileTrackCoudeItem = (props) => {
  const { productName, trackCode, status } = props;
  return (
    <div className={classes.trackCodeItem}>
      <Grid container alignItems={"center"}>
        <Grid item xs={10}>
          <Grid container mb={2}>
            <Grid xs={6}>
              <Button sx={{ color: "#0000ff", fontSize: 12 }}>Трек код</Button>
              <Grid padding={"0 8px"}>{trackCode}</Grid>
            </Grid>
            <Grid xs={6}>
              <Button sx={{ color: "#0000ff", fontSize: 12 }}>
                Название товара
              </Button>
              <Grid padding={"0 8px"}>{productName}</Grid>
            </Grid>
          </Grid>
          <Grid xs={12}>
            <Button
              sx={{
                backgroundColor: "transparent",
                color: "#0000ff",
                fontSize: 12,
              }}
              endIcon={<DownSvg />}
            >
              Статус
            </Button>
            <StyledContainer padding={"0 8px"}>
              {statuses[status]}
            </StyledContainer>
          </Grid>
        </Grid>
        <Grid item xs={2}>
          <Box sx={{ display: "flex", marginRight: "20px" }}>
            <DoneSvg />
          </Box>
        </Grid>
      </Grid>
    </div>
  );
};
