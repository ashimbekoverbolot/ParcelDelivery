import styled from "@emotion/styled";
import { Box, Button, CircularProgress, Grid } from "@mui/material";
import { useEffect, useState } from "react";
import { ReactComponent as DownSvg } from "../../assets/icons/down.svg";
import { ReactComponent as MinusSvg } from "../../assets/icons/minus.svg";
import TrackCodeItem from "./TracCodeItem";

import { BrowserView, MobileView } from "react-device-detect";
import { MobileTrackCoudeItem } from "./MobileTrackCodeItem";

const StyledButton = styled(Button)({
  ".MuiButton-startIcon": {
    marginRight: "20px",
  },
});

const TrackCodeList = (props) => {
  const { isAdmin, loading } = props;
  const [data, setData] = useState(props.data);

  useEffect(() => {
    setData(props.data);
  }, [props.data]);

  const handleSort = async () => {
    const filteredData = data.filter((track) => track.status === "SCH");
    setData(filteredData);
  };

  return (
    <>
      <BrowserView>
        <Box bgcolor="#FCFCFD" width="100vw" marginLeft="-24px" mt={3} p="12px">
          <Grid container>
            <Grid item xs={isAdmin ? 2 : 4} pl="26px">
              <StyledButton
                sx={{ backgroundColor: "transparent", color: "#0000ff" }}
                startIcon={<MinusSvg />}
                onClick={() => {
                  data.forEach((item) => {
                    if (props.onCollect) {
                      props.onCollect({
                        trackCode: item.track_code,
                      });
                    }
                  });
                }}
              >
                Трек код
              </StyledButton>
            </Grid>
            <Grid item xs={isAdmin ? 1.5 : 3}>
              <Button
                sx={{ backgroundColor: "transparent", color: "#0000ff" }}
                endIcon={<DownSvg />}
              >
                Статус
              </Button>
            </Grid>
            <Grid item alignSelf={"flex-start"} xs={isAdmin ? 1 : 3}>
              <Button sx={{ backgroundColor: "transparent", color: "#0000ff" }}>
                Название товара
              </Button>
            </Grid>
            {isAdmin && (
              <>
                <Grid item xs={isAdmin ? 1 : 4} ml="12px">
                  <Button
                    sx={{ backgroundColor: "transparent", color: "#0000ff" }}
                    onClick={handleSort}
                  >
                    На складе в Китае
                  </Button>
                </Grid>
                <Grid item xs={isAdmin ? 1 : 4} ml="12px">
                  <Button
                    sx={{ backgroundColor: "transparent", color: "#0000ff" }}
                  >
                    Код клиента
                  </Button>
                </Grid>
                <Grid item xs={isAdmin ? 2 : 4} ml="12px">
                  <Button
                    sx={{ backgroundColor: "transparent", color: "#0000ff" }}
                  >
                    Номер телефона клиента
                  </Button>
                </Grid>
              </>
            )}
          </Grid>
        </Box>
        {loading ? (
          <CircularProgress />
        ) : (
          data?.length > 0 &&
          data?.map((data) => {
            return (
              <TrackCodeItem
                key={data.track_code}
                trackCode={data.track_code}
                status={data.status}
                productName={data.title}
                userCode={data.user.user_code ?? "-----------"}
                phoneNumber={data.user.phone_number ?? "-----------"}
                isAdmin={isAdmin}
                setData={setData}
                joinedTimeInChina={data.joined_in_china}
                onCollect={({ trackCode }) => {
                  if (props.onCollect) {
                    props.onCollect({ trackCode });
                  }
                }}
                isSelected={data.isSelected}
              />
            );
          })
        )}
      </BrowserView>
      <MobileView>
        {data?.length > 0 &&
          data?.map((data) => {
            return (
              <MobileTrackCoudeItem
                productName={data.title}
                trackCode={data.track_code}
                status={data.status}
              />
            );
          })}
      </MobileView>
    </>
  );
};

export default TrackCodeList;
