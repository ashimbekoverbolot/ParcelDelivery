import {
  Box,
  Button,
  Checkbox,
  FormControl,
  Grid,
  MenuItem,
  Select,
  Typography,
} from "@mui/material";

import styled from "@emotion/styled";
import { useState } from "react";

import UserAPI from "../../api/user-api";
import { statuses } from "../../constants/statuses";

import EditIcon from "@mui/icons-material/Edit";
import SaveIcon from "@mui/icons-material/Save";
import { ReactComponent as DoneSvg } from "../../assets/icons/done.svg";
import { UserInfo } from "../../view/ProfilView/components/UserInfo";

const StyledContainer = styled("div")({
  border: "1px solid #0000ff",
  borderRadius: 5,
  padding: "2px 15px",
  width: "fit-content",
});

const StyledSelect = styled(Select)({
  width: "100%",
  height: 40,

  "& .MuiSelect-outlined": {
    padding: "5px 10px",
    width: "100%",
  },
});

const TrackCodeItem = (props) => {
  const {
    trackCode,
    productName,
    status,
    isAdmin,
    userCode,
    phoneNumber,
    onCollect,
    joinedTimeInChina,
    isSelected,
  } = props;
  const [value, setvalue] = useState(status);
  const [track_code, setTrack_code] = useState(trackCode);
  const [isTrackCodeEditing, setIsTrackCodeEditing] = useState(false);
  const [product_name, setProduct_Name] = useState(
    productName ?? "------------"
  );
  const [isProcutNameEditing, setProductNameEditing] = useState(false);

  console.log(isAdmin);

  const handleChange = async (event) => {
    setvalue(event.target.value);
    await UserAPI.updateTrackStatus({
      trackCode: String(trackCode),
      status: event.target.value,
    });

    const { data } = await UserAPI.getPosts();

    props.setData(data);
  };

  const handleChangeTrackCode = async (event) => {
    try {
      await UserAPI.updateTrackCodeStatus({
        trackCode: String(trackCode),
        newTrackCode: track_code,
      });
      setIsTrackCodeEditing(false);
    } catch (error) {
      console.error(error);
    }
  };

  const handleProductName = async (event) => {
    try {
      await UserAPI.updateTrackCodeStatus({
        trackCode: String(trackCode),
        title: product_name,
        newTrackCode: track_code,
      });
      setProductNameEditing(false);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Box
      width="100%"
      marginLeft="-24px"
      p="0 20px "
      borderBottom="1px solid #EAECF0;"
    >
      <Grid container padding="18px 0">
        <Grid
          item
          xs={isAdmin ? 2 : 4}
          ml="8px"
          alignItems="center"
          display="flex "
        >
          {isAdmin ? (
            <Checkbox
              onChange={() => {
                onCollect({ trackCode });
              }}
              checked={isSelected}
            />
          ) : (
            <Box sx={{ display: "flex", marginRight: "20px" }}>
              <DoneSvg />
            </Box>
          )}

          <Typography
            component={"span"}
            sx={{
              overflowWrap: "anywhere",
              display: "flex",
              alignItems: "center",
              justifyContent: " space-between",
              width: "100%",
              paddingRight: "20px",
            }}
          >
            {isTrackCodeEditing ? (
              <UserInfo
                mb={false}
                subtitle={track_code}
                isEditing={isTrackCodeEditing}
                onChange={(value) => {
                  setTrack_code(value);
                }}
              />
            ) : (
              track_code
            )}

            <Button
              onClick={() => {
                isTrackCodeEditing
                  ? handleChangeTrackCode()
                  : setIsTrackCodeEditing(true);
              }}
            >
              {isTrackCodeEditing ? <SaveIcon /> : <EditIcon />}
            </Button>
          </Typography>
        </Grid>

        <Grid item xs={isAdmin ? 1.5 : 3}>
          {isAdmin ? (
            <Box sx={{ minWidth: 110 }}>
              <FormControl fullWidth>
                <StyledSelect
                  id="demo-simple-select"
                  value={value}
                  onChange={handleChange}
                >
                  <MenuItem value={"NST"}>Нет статуса</MenuItem>
                  <MenuItem value={"SCH"}>На складе в Китае</MenuItem>
                  <MenuItem value={"SKA"}>На складе в Казахстане</MenuItem>
                  <MenuItem value={"SNT"}>Отправлено</MenuItem>
                  <MenuItem value={"DLV"}>Доставлено</MenuItem>
                </StyledSelect>
              </FormControl>
            </Box>
          ) : (
            <StyledContainer>{statuses?.[status]}</StyledContainer>
          )}
        </Grid>
        <Grid item xs={isAdmin ? 1 : 3} marginLeft={2}>
          <Typography
            component={"span"}
            sx={{
              overflowWrap: "anywhere",
              display: "flex",
              alignItems: "center",
              justifyContent: " space-between",
              width: "100%",
              paddingRight: "20px",
            }}
          >
            {isProcutNameEditing ? (
              <UserInfo
                mb={false}
                subtitle={product_name}
                isEditing={isProcutNameEditing}
                onChange={(value) => {
                  setProduct_Name(value);
                }}
              />
            ) : (
              product_name
            )}

            <Button
              onClick={() => {
                isProcutNameEditing
                  ? handleProductName()
                  : setProductNameEditing(true);
              }}
            >
              {isProcutNameEditing ? <SaveIcon /> : <EditIcon />}
            </Button>
          </Typography>
        </Grid>

        {isAdmin && (
          <>
            <Grid
              item
              xs={isAdmin ? 1 : 4}
              ml="12px"
              alignItems="center"
              display="flex"
              marginLeft={2}
            >
              <Typography component={"span"} sx={{ overflowWrap: "anywhere" }}>
                {joinedTimeInChina}
              </Typography>
            </Grid>
            <Grid
              item
              xs={isAdmin ? 1 : 4}
              ml="12px"
              alignItems="center"
              display="flex"
              marginLeft={2}
            >
              <Typography component={"span"} sx={{ overflowWrap: "anywhere" }}>
                {userCode}
              </Typography>
            </Grid>
            <Grid
              item
              xs={isAdmin ? 2 : 4}
              ml="12px"
              alignItems="center"
              display="flex"
              marginLeft={1}
            >
              <Typography component={"span"} sx={{ overflowWrap: "anywhere" }}>
                {phoneNumber}
              </Typography>
            </Grid>
          </>
        )}
      </Grid>
    </Box>
  );
};

export default TrackCodeItem;
