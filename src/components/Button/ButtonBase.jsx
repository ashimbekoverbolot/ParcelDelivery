import styled from "@emotion/styled";
import { Button as MuiButton } from "@mui/material";

const StyledButton = styled(MuiButton, {
  shouldForwardProp: (prop) => prop !== "sx",
})(({ sx }) => ({
  background: "#0000ff",
  borderRadius: "8px",
  border: "none",
  color: "#ffffff",
  cursor: "pointer",
  transition: "all 0.3s linear ",
  height: 40,
  padding: "10px 24px",
  ...(sx && {
    ...sx,
  }),

  "&:hover": {
    backgroundColor: "#0000ff",
    borderColor: "#0000ff",
    boxShadow: "none",
  },
}));

const ButtonBase = (props) => {
  const { onClick, ...rest } = props;

  return (
    <StyledButton variant="contained" onClick={onClick} {...rest}>
      {props.children}
    </StyledButton>
  );
};

export default ButtonBase;
