import styled from "@emotion/styled";
import ButtonBase from "./ButtonBase";

const StyledButton = styled(ButtonBase)(() => ({
  width: 205,
  height: 50,
  padding: "15px 75px",
  marginBottom: "15px",
}));

const SignButton = (props) => {
  const { onClick, ...rest } = props;

  return (
    <StyledButton variant="contained" onClick={onClick} {...rest}>
      {props.children}
    </StyledButton>
  );
};

export default SignButton;
