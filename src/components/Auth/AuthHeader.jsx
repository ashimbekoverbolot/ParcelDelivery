import classes from "./AuthHeader.module.scss";

import Logo from "../../assets/logo.png";
import Link from "../Link/Link";
import SignButton from "../Button/SignButton";
import { CircularProgress } from "@mui/material";

const AuthHeader = (props) => {
  const {
    submitButton = { onClick: () => {}, text: "Войти", type: "button" },
    title = "Вход",
    children,
    signUp,
    loading,
  } = props;

  return (
    <>
      <div className={classes.container}>
        <div className={classes.imageWrapper}>
          <img src={Logo} alt="logo" className={classes.image} />
        </div>
        <h1 style={{ marginBottom: "20px" }}>{title}</h1>
        <div className={classes.bodyWrapper}>{children}</div>

        <SignButton
          type={submitButton.type}
          onClick={submitButton.onClick}
          variant={"signIn"}
        >
          {submitButton.text}
        </SignButton>

        {loading && <CircularProgress />}
        {signUp?.title && (
          <Link href={signUp.href} className={classes.signUp}>
            {signUp.title}
          </Link>
        )}
      </div>
    </>
  );
};

export default AuthHeader;
