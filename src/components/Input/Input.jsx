import styled from "@emotion/styled";
import { Input as MuiInput } from "@mui/material";
import classes from "./Input.module.scss";

const StyledInput = styled(MuiInput)(() => ({
  ".MuiInput-input": {
    height: 38,
    border: "1px solid #c9c9c9",
    borderRadius: 5,
    padding: "0 5px",
  },

  "&:before": {
    borderBottom: "5px solid transparent",
    borderRadius: "0 0 3px 3px",
  },

  "&:hover:not(.Mui-disabled, .Mui-error):before": {
    borderBottom: "5px solid #FE5D03",
    borderRadius: "0 0 3px 3px",
  },

  "&:after": {
    borderBottom: "5px solid #FE5D03",
    borderRadius: "0 0 3px 3px",
  },
}));

const Input = (props) => {
  const { label, name, ...rest } = props;

  return (
    <>
      <div className={classes.inputWrapper}>
        <label htmlFor={label}>{label}</label>
        <StyledInput
          variant="outlined"
          className={classes.input}
          name={name}
          {...rest}
        />
      </div>
    </>
  );
};

export default Input;
