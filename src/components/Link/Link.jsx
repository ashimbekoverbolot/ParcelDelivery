import styled from "@emotion/styled";
import { Link as MuiLink } from "@mui/material";

const Link = (props) => {
  const { href, children } = props;

  const StyledLink = styled(MuiLink)(() => ({
    textDecoration: "none",
    color: "#000000",
    marginTop: "26px",
    paddingBottom: "5px",
    borderBottom: "1px solid #000000",
  }));

  return <StyledLink href={href}>{children}</StyledLink>;
};

export default Link;
