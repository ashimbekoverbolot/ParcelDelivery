import { createTheme } from "@mui/material";

export const theme = createTheme({
  palette: {
    primary: {
      main: "#FE5D03",
    },
    secondary: {
      main: "#1D2939",
    },
    color3: {
      main: "#0000ff",
    },
    color4: {
      main: "#0000ff",
    },
  },
});
