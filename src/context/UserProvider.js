import { createContext, useContext, useState } from "react";

export const UserContext = createContext();

const UserProvider = (props) => {
  const [userData, setUserData] = useState({});

  const value = { userData, setUserData };

  return (
    <UserContext.Provider value={value}>{props.children}</UserContext.Provider>
  );
};

export default UserProvider;

export const useUser = () => {
  return useContext(UserContext);
};
